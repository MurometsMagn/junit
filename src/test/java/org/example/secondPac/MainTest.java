package org.example.secondPac;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.*;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class MainTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testPrime() {
        assertEquals(4, 2 + 2);
        assertTrue(Main.isPrime(7)); //протестироывать на разн парам
        assertFalse(Main.isPrime(8)); //протестироывать на разн парам
        //assertEquals(new ArrayList<Integer>(2, 3, 5, 7), Main.primesBetween(2, 10));
        //assertEquals(new int[]{2, 3, 5, 7}, Main.primesBetween(2, 10).toArray());
    }

    @Test
    public void testPrimesBetween() {
        List<Integer> primes = Main.primesBetween(2, 10);
        // System.out.println(primes.toArray());
        for (int p : primes) {
            System.out.println(p);
        }
        assertThat(primes, is(Arrays.asList(2, 3, 5, 7)));
        //
    }

    @Test
    public void testPrimesBetween2() {
        List<Integer> primes = Main.primesBetween(2, 10);
        Integer[] asIs = new Integer[primes.size()];
        primes.toArray(asIs);
        assertArrayEquals(new Integer[]{2, 3, 5, 7}, asIs);
    }

    @Test
    public void testTag() {
        assertEquals("<p>3</p>", Main.tag(3));
        assertEquals("<p class=\"red\">7</p>", Main.tag(7));
    }

    @Test
    public void testReaderWriter() { //интеграционный тест
        WritingFile writingFile = new WritingFile(new File("testFile.txt"));
        writingFile.writing(Arrays.asList(1, 2, 3, 4, 5));

        Main2.readingWriting(
                new ReadingFile(new File("testFile.txt")),
                new WritingFile(new File("testResultFile.txt"))
        );

        //дописать тест.
        //что будет влиять на воспроизводимость теста (что может пойти не так)

        List<Integer> readList = new ReadingFile(new File("testResultFile.txt")).reading();
        assertThat(readList, is(Arrays.asList(2, 4, 6, 8, 10)));
    }

    @Test
    public void testReaderWriter2() { //модульный тест
        List<Integer> data = Arrays.asList(1, 2, 3, 4, 5);

        WritingFileTest writingFileTest = new WritingFileTest();
        Main2.readingWriting(new ReadingFileTest(data), writingFileTest);
        assertThat(writingFileTest.data, is(Arrays.asList(2, 4, 6, 8, 10)));
    }

   /* @Test
    public void coping() {
        Main2.copy();
    }*/
}