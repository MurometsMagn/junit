package org.example.secondPac;

import org.example.secondPac.IReadingFile;

import java.util.List;

public class ReadingFileTest implements IReadingFile {
    private List<Integer> testData;

    public ReadingFileTest(List<Integer> testData) {
        this.testData = testData;
    }

    @Override
    public List<Integer> reading() {
        return testData;
    }
}
