package org.example.thirdPac;

import org.example.thirdPac.DataBase;
import org.example.thirdPac.IDataBase;
import org.junit.Test;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;

public class DataBaseTest {
    private Connection connection;

    @Test
    public void loginTest() throws SQLException {
        DataBase dataBase = new DataBase("jdbc:sqlite::memory:");
        assertTrue(dataBase.login("admin", "admin"));
        assertFalse(dataBase.login("muromets","passw"));
    }

    @Test
    public void generateLoginSQLTest() {
        assertEquals(
                "SELECT id,name,password FROM users WHERE name = 'admin' AND password = 'admin'",
                DataBase.generateLoginSQL("admin", "admin")
        );
    }

    @Test
    public void doLoginTest() throws SQLException { //интеграционное
        DataBase dataBase = new DataBase("jdbc:sqlite::memory:");
        String st1 = "admin\nadmin";
        String st2 = "muromets\npassword";

        //Оборачиваем строку в класс ByteArrayInputStream
        InputStream is1 = new ByteArrayInputStream(st1.getBytes());
        InputStream is2 = new ByteArrayInputStream(st2.getBytes());

        //подменяем in
        System.setIn(is1);

        assertTrue(Main3.doLogin(dataBase)); //проверяются admin-admin

        System.setIn(is2);
        assertFalse(Main3.doLogin(dataBase)); //проверяются muromets-password
    }

    @Test
    public void doLoginTest2() throws SQLException { //почти модульное (не лезим в БД)
        IDataBase dataBase = new DataBaseMock();
        String st1 = "admin\nadmin";
        String st2 = "muromets\npassword";

        //Оборачиваем строку в класс ByteArrayInputStream
        InputStream is1 = new ByteArrayInputStream(st1.getBytes());
        InputStream is2 = new ByteArrayInputStream(st2.getBytes());

        //подменяем in
        System.setIn(is1);

        assertTrue(Main3.doLogin(dataBase)); //проверяются admin-admin

        System.setIn(is2);
        assertFalse(Main3.doLogin(dataBase)); //проверяются muromets-password
    }
}

//
