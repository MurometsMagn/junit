package org.example.thirdPac;

import java.sql.SQLException;

public class DataBaseMock implements IDataBase { //мок-класс имитирует реальн объект и возвр-ет тестовые данные
    @Override
    public boolean login(String userName, String password) {
        return userName.equals("admin") && password.equals("admin");
    }

}
