package org.example.thirdPac;

//import org.example.KotlinProbFileKt;

import java.sql.*;
import java.util.Scanner;

/**
 * Система авторизации запрашивает у пользователя логин и пароль.
 * Для заданного логина и пароля формируется SQL запрос и делается запрос в базу данных.
 * Если пользователь с такими параметрами существует, он возвращается. Если его не существует --
 * возбуждается исключительная ситуация NoSuchUserException.
 * Написать данную систему и протестировать в соответствии с принципами
 * модульного тестирования компоненты генерации SQL запроса и обращения к БД
 */

/**
 * Заключение:
 * В случае, если sql-запрос должен содержать данные из небезопасных источников (напр пользоватьельский ввод),
 * то вместо объектов Statement лучше использовать объект PrepearedStatement,
 * в этом случае на месте небезопасных данных мы указываем символ ? (вопросительный знак),
 * а сами небезопасные данные указываем  позднее при помощи метода PrepearedStatement.
 */

public class Main3 {
    public static void main(String[] args) throws SQLException {

        //System.out.println(KotlinProbFileKt.getX());

        /*try (Connection conn = DriverManager.getConnection("jdbc:sqlite::memory:")) {
            Statement stmt = conn.createStatement();
            stmt.executeUpdate("CREATE TABLE Users (id integer PRIMARY KEY, name varchar(50), password varchar(50))");
            stmt.executeUpdate("INSERT INTO Users values (1, \"admin\", \"admin\")");
            stmt.executeUpdate("INSERT INTO Users values (2, \"test\", \"test\")");


            ResultSet rs = stmt.executeQuery("SELECT * FROM Users");
            while (rs.next()) {
                System.out.println(rs.getInt("id"));
                System.out.println(rs.getString("name"));
                System.out.println(rs.getString("password"));
            }
        }*/

        DataBase dataBase = new DataBase("jdbc:sqlite::memory:");
        if (doLogin(dataBase)) {
            System.out.println("authentication OK");
        } else {
            System.out.println("authentication failed");
        }
    }
    //executeQuery - возвращают данные
    //executeUpdate - не возвращает данные

    public static boolean doLogin(IDataBase dataBase) {
        Scanner scanner = new Scanner(System.in);
        String login;
        String password;

        System.out.println("input login:");
        login = scanner.nextLine();
        System.out.println(login);

        System.out.println("input password:");
        password = scanner.nextLine();
        System.out.println(password);

        return dataBase.login(login, password);
    }

    //протестить ф-ю doLogin ср-вами модульного тест-я
    //сделать общ предка у DB
    //унаследовать от него класс DB
    //cоздать тестовый наследник TestDB
    //понадобятся System.setIn и
    //ByteArrayInputStream
}
