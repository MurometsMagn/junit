package org.example.thirdPac;

import java.sql.*;

/**
 * SELECT id,login,password FROM users WHERE login = 'admin' AND password = 'admin';
 */

public class DataBase implements IDataBase{
    private Connection connection;

    public DataBase(String jdbcUrl) throws SQLException {
        connection = DriverManager.getConnection(jdbcUrl);
        Statement stmt = connection.createStatement();
        stmt.executeUpdate("CREATE TABLE Users (id integer PRIMARY KEY, name varchar(50), password varchar(50))");
        stmt.executeUpdate("INSERT INTO Users values (1, 'admin', 'admin')");
        stmt.executeUpdate("INSERT INTO Users values (2, \"test\", \"test\")");
    }

    public boolean login(String userName, String password) {
        //String loginSQL = generateLoginSQL(userName, password);
        try {
            //Statement stmt = connection.createStatement();
            //ResultSet rs = stmt.executeQuery(loginSQL);
            PreparedStatement stmt = connection.prepareStatement(
                    "SELECT id,name,password FROM users WHERE name = ? AND password = ?"
            );
            stmt.setString(1, userName);
            stmt.setString(2, password);
            ResultSet rs = stmt.executeQuery();
           /* if (rs.next()) {
                return true;
            } else return false;*/
            return rs.next(); //simplified
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
        //return true;

        //дописать ф-ю
        //протестить login() и generateLoginSQL
    }

    public static String generateLoginSQL(String userName, String password) { //не исп-ть в реальном коде всл-е опастности sql-инъекции
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT id,name,password FROM users WHERE name = '");
        queryBuilder.append(userName);
        queryBuilder.append("' AND password = '");
        queryBuilder.append(password);
        queryBuilder.append("'");

        //return String.valueOf(queryBuilder);
        return queryBuilder.toString();
    }
}

// admin' AND 1 --      sqlinjection
