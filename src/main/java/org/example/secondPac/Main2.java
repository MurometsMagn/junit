package org.example.secondPac;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

public class Main2 {
    public static void main(String[] args) {
        /*Написать программу, в которой исходные данные берутся из текстового файла. В этом файле расположены
        несколько чисел каждое на своей строке. На первой строке написано количество чисел. Пример файла:

4
12
45
87
102


Каждое прочитанное число из файла умножить на 2 и вывести в отдельный текстовый файл того же формата.
4
24
90
174
204
         */
        //чтение и запись вынести в отдельные классы

       /* File file = new File("filename.txt");

        try (Scanner s = new Scanner(file)){
            //File file = new File("filename.txt");
            //Scanner s = new Scanner(file);
            while (s.hasNext()) { // s.hasNextInt(), и т.д.
                System.out.println(s.next()); // s.nextInt(), и т.д.
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        }*/

        //File file = new File("filename.txt");
        //File resultFile = new File("resultFile.txt");

        //ReadingFile readingFile = new ReadingFile(file);


//        for (var x : inputData) {
//            System.out.println(x);
//        }


        //WritingFile writingFile = new WritingFile(resultFile);
        //writingFile.writing(Arrays.asList(4, 12, 45, 87, 102)); //инициализация листа
//        for (int i = 0; i < inputData.size(); i++) {
//            inputData.set(i, inputData.get(i) * 2);
//        }
        //writingFile.writing(inputData, resultFile);
        //readingWriting(readingFile, writingFile);
        readingWriting(
                new ReadingFile(new File("filename.txt")),
                new WritingFile(new File("resultFile.txt"))
        );
    }

    public static void readingWriting(IReadingFile readingFile, IWritingFile writingFile) {
        List<Integer> inputData = readingFile.reading();
        writingFile.writing(inputData.stream().map(x -> x * 2).collect(Collectors.toList())); //map,filter,reduce
    }
}
