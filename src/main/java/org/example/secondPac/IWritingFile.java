package org.example.secondPac;

import java.util.List;

public interface IWritingFile {
    void writing(List<Integer> outputData);
}
