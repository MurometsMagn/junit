package org.example.secondPac;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ReadingFile implements IReadingFile {
    private File file;

    public ReadingFile(File file) {
        this.file = file;
    }

    @Override
    public List<Integer> reading() {
        ArrayList<Integer> inputData = new ArrayList<>();
        try (Scanner s = new Scanner(file)) {
            int n = s.nextInt();
            for (int i = 0; i < n; i++) {
                inputData.add(s.nextInt());
            }
        } catch (
                FileNotFoundException e) {
            System.out.println("File not found");
        }
        return inputData;
    }
}
