package org.example.secondPac;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class WritingFile implements IWritingFile {
    private File file;

    public WritingFile(File file) {
        this.file = file;
    }

    @Override
    public void writing(List<Integer> outputData) {
        //File file = new File("filename.txt");
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            writer.write("" + outputData.size());
            for (var i : outputData) {
                writer.newLine();
                writer.write("" + i); //приведение к типу String
            }
        } catch (
                IOException e) {
            System.out.println("I/O error");
        }
    }
}
