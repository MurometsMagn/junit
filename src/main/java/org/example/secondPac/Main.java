package org.example.secondPac;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int firstNumber;
        int lastNumber;

        System.out.println("enter a first number");
        firstNumber = scanner.nextInt();

        System.out.println("enter a second number");
        lastNumber = scanner.nextInt();

        if (lastNumber < firstNumber) {
            int tmp = lastNumber;
            lastNumber = firstNumber;
            firstNumber = tmp;
        }

        for (int prime : primesBetween(firstNumber, lastNumber)) {
            System.out.println("prime " + tag(prime));
        }
    }

    static boolean isPrime(int inputNumber) {
        boolean isPrime = true;
        for (int j = 2; j <= Math.sqrt(inputNumber) && isPrime; j++) {
            if (inputNumber % j == 0) {
                isPrime = false;
            }
        }
        return isPrime;
    }

    static List<Integer> primesBetween(int firstNumber, int lastNumber) {
        ArrayList<Integer> primesArray = new ArrayList<>();
        for (int i = firstNumber; i <= lastNumber; i++) {
            boolean isPrime = isPrime(i);
            if (isPrime) {
                primesArray.add(i);
            }
        }
        return primesArray;
    }

    static String tag(int inputNumber) {
        String str;
        if (inputNumber <= 5) {
            str = ("<p>" + inputNumber + "</p>");
        } else {
            str = ("<p class=\"red\">" + inputNumber + "</p>");
        }
        return str;
    }
}
