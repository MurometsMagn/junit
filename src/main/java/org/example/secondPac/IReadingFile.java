package org.example.secondPac;

import java.util.List;

public interface IReadingFile {
    List<Integer> reading();
}
