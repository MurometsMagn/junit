package org.example.firstPac;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Geters {
    static class Test {
        private ArrayList<Integer> field = new ArrayList<>();

        public void add(int x) {
            field.add(x);
        }

        public ArrayList<Integer> getField() {
            return field;
        }
    }


    public static void main(String[] args) {
        Test t = new Test();
        t.add(10);
        t.getField().set(0, 42);
        System.out.println(Arrays.toString(t.getField().toArray()));
    }
}