package org.example.firstPac;

import java.util.ArrayList;

class Test {
    private final ArrayList<Integer> field = new ArrayList<>();

    public void add(int x) {
        field.add(x);
    }

    //        public ArrayList<Integer> getField() {
//            return field;
//        }
    public int getSize() {
        return field.size();
    }

    public int getElem(int index) {
        return field.get(index);
    }

    public static void main(String[] args) {
        Test t = new Test();
        t.add(10);
        //t.getField().set(0, 42);
        //System.out.println(Arrays.toString(t.getField().toArray()));
        for (int i = 0; i < t.getSize(); i++) {
            System.out.println(t.getElem(i));
        }
    }
}
